package com.example.shibe.view
import com.example.shibe.databinding.ActivityMainBinding
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shibe.R
import com.example.shibe.model.remote.ShibeRepo
import com.example.shibe.viewModel.ShibeVMFactory
import com.example.shibe.viewModel.ShibeViewModel

class MainActivity : AppCompatActivity(){
    lateinit var binding : ActivityMainBinding
    private val vmFactory:ShibeVMFactory = ShibeVMFactory(ShibeRepo(this.applicationContext))
    private val viewModel by viewModels<ShibeViewModel> { vmFactory  }

    private val theAdapter: ShibeAdapter by lazy{
        ShibeAdapter()
    }

    override fun onCreate(savedInstanceState:Bundle?){
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        initViews()
        initObservers()
        setContentView(binding.root)
        viewModel.getShibes()
    }



    private fun initViews(){
        with(binding.rvDoggy){
            layoutManager = GridLayoutManager(this@MainActivity,2)
            adapter = theAdapter
        }


    }
    private fun initObservers(){
        viewModel.shibes.observe(this){state->
            binding.progress.isVisible = state.isLoading
            theAdapter.updateList(state.shibes)
        }
    }
}

