package com.example.shibe.model.remote
import android.content.Context
import com.example.shibe.model.remote.ShibeService
import com.example.shibe.model.remote.local.Animal
import com.example.shibe.model.remote.local.AnimalDB
import com.example.shibe.utils.AnimalType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext


class ShibeRepo(context: Context) {

    private val shibeService = ShibeService.getInstance()
    private val shibeDao = AnimalDB.getInstance(context).animalDao()

    suspend fun getShibes(): List<Animal> = withContext(Dispatchers.IO) {
        val cachedShibes = shibeDao.getAllTypedAnimals(AnimalType.SHIBE)
        delay(1000)
        return@withContext cachedShibes.ifEmpty {
            //Go to the network
            val remoteDoggos = shibeService.getShibes()

            //Map our list to Strings to a shibe entity
            val entities: List<Animal> = remoteDoggos.map {
                Animal(type = AnimalType.SHIBE, image = it)
            }
            //Save the shibes
            shibeDao.insertAnimal(*entities.toTypedArray())
            //return a list of  all the shibes.
            return@ifEmpty entities
        }
    }
}