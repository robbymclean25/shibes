package com.example.shibe.model.remote.local
import androidx.room.RoomDatabase
import android.content.Context
import androidx.room.Database
import androidx.room.Room

@Database(entities = [Animal::class], version = 1)
abstract class AnimalDB: RoomDatabase() {
    abstract fun animalDao(): ShibeDAO
    companion object{
        private const val DATABASE_NAME = "animal.db"
        @Volatile
        private var instance : AnimalDB? = null

        fun getInstance(context: Context): AnimalDB {
            return instance?: synchronized( this){
                instance ?: buildDatabase(context).also{
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): AnimalDB {
            return Room
                .databaseBuilder(context,AnimalDB ::class.java, DATABASE_NAME)
                .build()

        }
    }

}