package com.example.shibe.model.remote.local

import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.shibe.utils.AnimalType

interface ShibeDAO {
    @Query("SELECT * FROM animal")
    suspend fun getAll() : List<Animal>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAnimal(vararg animal:Animal)

    @Query("SELECT * FROM Animal WHERE type = :animalType ")
    suspend fun getAllTypedAnimals(animalType: AnimalType): List<Animal>
}